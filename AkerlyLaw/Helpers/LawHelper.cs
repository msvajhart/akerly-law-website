﻿using AkerlyLaw.DBModels;
using AkerlyLaw.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Helpers
{
    public class LawHelper
    {
        public static FirmModel GetLaw(CopperheadContext _context)
        {
            List<LawModel> LawList = new List<LawModel>();


            //retrieve list of types of law from dbContext
            var query = _context.Law.Where(l => l.WebsiteID == 1);
            foreach (Law l in query)
            { 
                LawModel Law = new LawModel(l.Title, l.Icon, l.ShortDesc, l.Description, l.Slug);
                LawList.Add(Law);
            }

            FirmModel firm = new FirmModel
            {
                Law = LawList
            };

            return firm;
        }



        public static LawModel GetLawDetails(CopperheadContext _context, string slug)
        {
            var l = _context.Law.Where(x => x.Slug.ToLower() == slug.ToLower()).FirstOrDefault();

            if(l == null)
            {
                return null;
            }

            LawModel Law = new LawModel(l.Title, l.Icon, l.ShortDesc, l.Description, l.Slug);

            return Law;
        }


        public static LawModel GetLawDetailsBefore(CopperheadContext _context, string slug)
        {
            var l = _context.Law.Where(x => x.Slug.ToLower() == slug.ToLower() && x.WebsiteID == 1).FirstOrDefault();

            var v = _context.Law.Where(x => x.ID < l.ID && x.WebsiteID == 1).OrderByDescending(c => c.ID).FirstOrDefault();

            if (v == null)
            {
                return null;
            }

            LawModel Law = new LawModel(v.Title, v.Icon, v.ShortDesc, v.Description, v.Slug);

            return Law;
        }

        public static LawModel GetLawDetailsAfter(CopperheadContext _context, string slug)
        {
            var l = _context.Law.Where(x => x.Slug.ToLower() == slug.ToLower() && x.WebsiteID == 1).FirstOrDefault();

            var v = _context.Law.Where(x => x.ID > l.ID && x.WebsiteID == 1).OrderBy(x => x.ID).FirstOrDefault();

            if (v == null)
            {
                return null;
            }

            LawModel Law = new LawModel(v.Title, v.Icon, v.ShortDesc, v.Description, v.Slug);

            return Law;
        }


        public static LawChildren GetLawChildren(CopperheadContext _context, string slug)
        {
            List<LawChildModel> ChildrenList  = new List<LawChildModel>();

            var query = _context.LawChildrenDB.Join(_context.Law,
             lc => lc.LawID,
             l => l.ID,
             (lc, l) => new { LawChildrenDB = lc, Law = l })
             .Where(llc => llc.Law.Slug == slug && llc.Law.WebsiteID == 1)
             .Select(llc => llc.LawChildrenDB);


            foreach (LawChildrenDB c in query)
            {
                LawChildModel Child = new LawChildModel(c.Title, c.Description);
                ChildrenList.Add(Child);
            }

            LawChildren Children = new LawChildren
            {
                LawList = ChildrenList
            };

            return Children;
        }
    }
}
