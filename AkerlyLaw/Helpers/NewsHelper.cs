﻿using AkerlyLaw.DBModels;
using AkerlyLaw.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Helpers
{
    public class NewsHelper
    {
        public static NewsHomeViewModel GetNews(CopperheadContext _context)
        {
            List<NewsArticleModel> List = new List<NewsArticleModel>();

            //var query = _context.News.Where(t => t.WebsiteID == 1 && t.Published == true).OrderByDescending(x => x.Date); 

            var query = from n in _context.News
                        join nt in _context.NewsType on n.TypeID equals nt.ID
                        where (n.WebsiteID == 1 && n.Published == true)
                        orderby n.Date descending
                        select new { n.Title, n.Content, n.PreviewText, n.Slug, n.Date, n.Author, nt.TypeTag, nt.TypeTitle };

            foreach (var n in query)
            {

                NewsArticleModel Article = new NewsArticleModel(n.Title, n.Content, n.PreviewText, n.Slug, n.Author, n.Date, n.TypeTag, n.TypeTitle);
                List.Add(Article);
            }

            NewsHomeViewModel news = new NewsHomeViewModel
            {
                News = List
            };

            return news;
        }

        public static List<NewsArticleModel> GetTopNews(CopperheadContext _context)
        {
            List<NewsArticleModel> List = new List<NewsArticleModel>();

            //var query = _context.News.Where(t => t.WebsiteID == 1 && t.Published == true).OrderByDescending(x => x.Date); 

            var query = (from n in _context.News
                        join nt in _context.NewsType on n.TypeID equals nt.ID
                        where (n.WebsiteID == 1 && n.Published == true)
                        orderby n.Date descending
                        select new { n.Title, n.Content, n.PreviewText, n.Slug, n.Date, n.Author, nt.TypeTag, nt.TypeTitle }).Take(10);

            foreach (var n in query)
            {

                NewsArticleModel Article = new NewsArticleModel(n.Title, n.Content, n.PreviewText, n.Slug, n.Author, n.Date, n.TypeTag, n.TypeTitle);
                List.Add(Article);
            }

            return List;
        }

        public static NewsArticleModel GetArticle(CopperheadContext _context, string slug)
        {
            

            var n = _context.News.Where(i => i.Slug.ToLower() == slug.ToLower() && i.WebsiteID == 1 && i.Published == true).FirstOrDefault();

            NewsArticleModel Article = new NewsArticleModel(n.Title, n.Content, n.PreviewText, n.Slug, n.Author, n.Date, "", "");

            return Article;
        }
    }
}
