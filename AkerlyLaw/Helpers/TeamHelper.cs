﻿using AkerlyLaw.DBModels;
using AkerlyLaw.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Helpers
{
    public class TeamHelper
    {
        public static bool CheckTeam(TeamModel t, string slug)
        {
            foreach (MemberModel m in t.Team) // Loop through List with foreach
            {
                if (m.Slug == slug) return true;
            }
            return false;
        }

        public static TeamModel GetTeam(CopperheadContext _context)
        {
            List<MemberModel> Members = new List<MemberModel>();

                
            var query = from t in _context.Team
            where (t.WebsiteID == 1)
            select new { t.Name, t.Nickname, t.Slug, t.ImageName, t.FocuspointData, t.Role};



            foreach (var t in query)
            {

                MemberModel member = new MemberModel(t.Name, t.Nickname, t.Slug, t.Role, t.ImageName, t.FocuspointData, "");
                Members.Add(member);
            }

            TeamModel Team = new TeamModel
            {
                Team = Members
            };

            return Team;
        }
        public static MemberModel GetTeamMember(CopperheadContext _context, string slug)
        {
            var t = _context.Team.Where(m => m.Slug.ToLower() == slug.ToLower()).FirstOrDefault();

            
            MemberModel member = new MemberModel(t.Name, t.Nickname, t.Slug, t.Role, t.ImageName, t.FocuspointData, "");

            return member;
        }

        public static TeamMemberViewModel GetTeamMemberView(CopperheadContext _context, string slug)
        {
            var t = _context.Team.Where(m => m.Slug.ToLower() == slug.ToLower()).FirstOrDefault();

            if (t == null)
            {
                return null;
            }
            int memberID = t.ID;

            MemberModel member = new MemberModel(t.Name, t.Nickname, t.Slug, t.Role, t.ImageName, t.FocuspointData, t.Bio);

            List<TeamEducationModel> education = GetMyEducation(_context, memberID);
            List<TeamActivitiesModel> activities = GetMyActivities(_context, memberID);
            List<TeamCourtsModel> courts = GetMyCourts(_context, memberID);

            TeamMemberViewModel model = new TeamMemberViewModel
            {
                Member = member,
                Education = education,
                Activities = activities,
                Courts = courts
            };

            return model;
        }

        public static List<TeamEducationModel> GetMyEducation(CopperheadContext _context, int id)
        {
            List<TeamEducationModel> education = new List<TeamEducationModel>();

            var query = from a in _context.TeamEducation
                        join b in _context.WebsiteEducation on a.WebsiteEducationID equals b.ID
                        where (a.TeamID == id)
                        select new { b.University, b.Extra };

            foreach (var t in query)
            {

                TeamEducationModel e = new TeamEducationModel(t.University, t.Extra);
                education.Add(e);
            }



            return education;
        }
        public static List<TeamActivitiesModel> GetMyActivities(CopperheadContext _context, int id)
        {
            List<TeamActivitiesModel> activities = new List<TeamActivitiesModel>();

            var query = from a in _context.TeamExtras
                        where a.TeamID == id && a.Category == 1
                        select new { a.Data };

            foreach (var t in query)
            {

                TeamActivitiesModel e = new TeamActivitiesModel(t.Data);
                activities.Add(e);
            }

            return activities;
        }
        public static List<TeamCourtsModel> GetMyCourts(CopperheadContext _context, int id)
        {
            List<TeamCourtsModel> courts = new List<TeamCourtsModel>();

            var query = from a in _context.TeamExtras
                        where a.TeamID == id && a.Category == 2
                        select new { a.Data };

            foreach (var t in query)
            {

                TeamCourtsModel e = new TeamCourtsModel(t.Data);
                courts.Add(e);
            }

            return courts;
        }
    }
}
