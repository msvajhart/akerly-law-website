﻿function initDropdownItems() {
    $("body").on("mouseenter", ".nav__dropdown__container.nav__item", function () {
        if (window.innerWidth > 1000) {
            $(this).addClass("dropopen");
        }
    });
    $("body").on("mouseleave", ".nav__dropdown__container.nav__item", function () {
        if (window.innerWidth > 1000) {
            $(this).removeClass("dropopen");
        }
    });
    $("body").on("click", ".nav__dropdown__container.nav__item", function () {
        if (window.innerWidth < 1000) {
            if ($(this).hasClass("dropopen")) {
                $(this).removeClass("dropopen");
            } else {
                $(this).addClass("dropopen");
            }
        }
    });
}

//function initializes focuspoint
function initFocuspoint(){
    $('.focuspoint').focusPoint({
        throttleDuration: 200
    });
}

//function initializes the sticky tracker
function initSticky() {
    $(window).scroll(function () {
        if ($(document).scrollTop() > 0 && !$("nav").hasClass("is__sticky")) {
            $("nav").addClass("is__sticky");
            setTimeout(function () { navPadding(); }, 200);
        } else if ($(document).scrollTop() == 0 && $("nav").hasClass("is__sticky")) {
            $("nav").removeClass("is__sticky");
            setTimeout(function () { navPadding(); }, 200);
            
        }
    });
}

function navPadding() {
   
    if ($(document).innerWidth() > 768) {
         //set padding behind the nav
        let h_nav = $("nav").innerHeight();
        $(".nav__padding").css("height", h_nav);

        //set the button height;
   
        let h = $(".nav__logo").innerHeight();
        let font = $(".nav__item").css('font-size');
        font = parseInt(font.replace("px", ''));
        h = (h - font) / 2;
        $(".nav__item").css("padding", h + "px 0");
    } else {
        $(".nav__item").css("padding", "12px 0");
        $(".nav__padding").css("height", "50px");
    }
}

function initMenu(){
    $("body").on("click", ".nav__button", function () {
        $(this).parent().toggleClass("open");
    });
}


function initSlickItems() {
    $('.splash__slider').slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $('.law__container').slick({
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 950,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false,
                    arrows: false,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: false,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.team__container').slick({
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false,
                    arrows: false,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: false
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.news__slider').slick({
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true,
                    arrows: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
}
function initContactPartial() {
    $(".member__contact__form").click(function () {
        $(".model__container .model").load("/Form/GetPartial/" + $(this).data("info"));
        $(".model__container").addClass("active");
    });

    $(".model__container .close").click(function () {
        console.log("hello")
        $(".model__container .model").empty();
        $(".model__container").removeClass("active");
    });
}


function initBlockMatching() {
    matchHeight(".op__law__title", false);
    matchHeight(".op__law__contents", false);
    matchHeight(".member__info", false);
    matchHeight(".law__child__desc", true);
    //matchHeight(".tm__block__details__second", false);
    matchHeight(".article__box h2", true);
    matchHeight(".article__short__desc", false)
}

function matchHeight(name, center) {
    TitleArr = $(name);
    var h = 0, maxh = 0;
    for (let i = 0; i < TitleArr.length; i++) {
        if (TitleArr[i].clientHeight > maxh) {
            maxh = TitleArr[i].clientHeight
        }
    }

    
    for (let i = 0; i < TitleArr.length; i++) {
        if (TitleArr[i].clientHeight < maxh) {
            if (center) {
                let setH = ((maxh - TitleArr[i].clientHeight) / 2);
                TitleArr[i].style.paddingBottom = setH + "px";
                TitleArr[i].style.paddingTop = setH + "px";            }
            else {
                let setH = (maxh - TitleArr[i].clientHeight);
                TitleArr[i].style.paddingBottom = setH + "px";
            }
        }
    }
}


var newsFilter = {
    init: function () {
        $('body').on("change", ".filters .toggleItem", function () {
            newsFilter.filter(this);
        });
        $('body').on("change", ".filters .toggleAll", function () {
            newsFilter.toggleFilters(this);
        });
    },
    filter: function ($this) {
        console.log($this.value);
        var articles = $(".nrcontainer a")
        for (var i = 0; i < articles.length; i++) {
            if ($this.checked) {
                if (articles[i].dataset.type == $this.value) {
                    articles[i].classList.remove("hidden");
                }
            } else {
                if (articles[i].dataset.type == $this.value) {
                    articles[i].classList.add("hidden");
                }
            }
        }
        if (!$this.checked) {
            $(".toggleAll").prop('checked', false);
        }
    },
    toggleFilters: function ($this) {
        if ($this.checked) {
            var toggles = $(".toggleItem");
            for (var i = 0; i < toggles.length; i++) {
                toggles[i].checked = true;
                newsFilter.filter(toggles[i])
            }
        }
    }

}




$(document).ready(function () {
    initFocuspoint();
    initSticky();
    navPadding();
    initMenu();
    initSlickItems();
    initContactPartial();
    initBlockMatching();
    newsFilter.init();
    initDropdownItems();
});

$(window).resize(function () {
    navPadding();
});