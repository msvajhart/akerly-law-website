﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkerlyLaw.DBModels;
using AkerlyLaw.Models;
using Microsoft.AspNetCore.Mvc;
using AkerlyLaw.Helpers;

namespace AkerlyLaw.Controllers
{
    public class TeamController : Controller
    {

        private readonly CopperheadContext _context;

        public TeamController(CopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {  

            TeamModel Team = TeamHelper.GetTeam(_context);

            return View(Team);
        }

        public IActionResult Member(string id)
        {
            TeamMemberViewModel Model = TeamHelper.GetTeamMemberView(_context, id);

            if (Model.Member == null)
            {
                return RedirectToAction("Index", "Team");
            }
            
            return View(Model);

        }
    }
}