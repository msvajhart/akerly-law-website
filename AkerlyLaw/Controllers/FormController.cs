﻿
using AkerlyLaw.Models;
using MailKit.Security;
using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;
using AkerlyLaw.Helpers;
using Microsoft.Extensions.Configuration;
using AkerlyLaw.DBModels;

namespace AkerlyLaw.Controllers
{
    public class FormController : Controller
    {

        private IConfiguration configuration;

        private CopperheadContext _context;


        public FormController(IConfiguration iConfig, CopperheadContext context)
        {
            configuration = iConfig;
            _context = context;
        }

        public IActionResult GetPartial(string id)
        {
            ContactFormModel model = new ContactFormModel() {
                Page = id 
            };

            return PartialView("_ContactForm", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<IActionResult> Contact(ContactFormModel Contact)
        {
            string secret = configuration.GetSection("GoogleReCaptcha").GetSection("secret").Value;

            if (!await GoogleRecaptchaHeler.IsReCaptchaPassedAsync(Request.Form["g-recaptcha-response"], secret))
            {
                //error
                //ModelState.AddModelError(string.Empty, "You failed the CAPTCHA");
                //return View(model);
                RedirectToAction("Index", "Home");
            }
            SaveLead(Contact);
            Sendmail(Contact);

            return RedirectToAction("Confirmation");
        }

        public IActionResult Confirmation(ContactFormModel Contact)
        {

            return View();
        }

        public void Sendmail(ContactFormModel Contact)
        {
            var messageToSend = new MimeMessage();

            messageToSend.From.Add(new MailboxAddress("website-lead-no-reply", "akerly.law.lead@gmail.com"));
            messageToSend.To.Add(new MailboxAddress("Akerly-Info", "info@akerlylaw.com"));
            messageToSend.To.Add(new MailboxAddress("Shelly", "sakerly@akerlylaw.com"));
            messageToSend.To.Add(new MailboxAddress("Michael-Test", "msvajhart@outlook.com"));
            //messageToSend.To.Add(new MailboxAddress("leadbox2", "michael.svajhart@okstate.edu"));
            messageToSend.Subject = "Form Completed By: " + Contact.Name;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Name: {0}", Contact.Name);
            sb.AppendLine();
            if (Contact.Page != null)
            {
                sb.AppendFormat("Page Details: {0}", Contact.Page);
                sb.AppendLine();
            }
            sb.AppendFormat("Email: {0}", Contact.Email);
            sb.AppendLine();
            sb.AppendFormat("Phone: {0}", Contact.Phone);
            sb.AppendLine();
            sb.AppendFormat("Company: {0}", Contact.Company);
            sb.AppendLine();
            sb.AppendFormat("Address: {0} {1} {2} {3} {4}", Contact.Address, Contact.City, Contact.State, Contact.Zip, Contact.Country);
            sb.AppendLine();
            sb.AppendLine("Information:");
            sb.AppendFormat("{0}", Contact.Description);


            messageToSend.Body = new TextPart(TextFormat.Plain) { Text = sb.ToString() };

            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                smtp.Connect("smtp.gmail.com", 587);
                smtp.Authenticate("akerly.law.lead@gmail.com", "ihuevgyaeklepwjx");
                try
                {
                    smtp.Send(messageToSend);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                          ex.ToString());
                }

                smtp.Disconnect(true);
            }

        }


        public void SaveLead(ContactFormModel Contact)
        {
            Leads lead = new Leads
            {
                WebsiteID = 1,
                Name = Contact.Name,
                Email = Contact.Email,
                Phone = Contact.Phone,
                Company = Contact.Company,
                Address = Contact.Address,
                City = Contact.City,
                State = Contact.State,
                ZipCode = Contact.Zip,
                Country = Contact.Country,
                Comments = Contact.Description
            };

            _context.Leads.Add(lead);
            _context.SaveChanges();
        }

        public void SendContactEmail(ContactFormModel Contact){

            Leads lead = new Leads
            {
                WebsiteID = 1,
                Name = Contact.Name,
                Email = Contact.Email,
                Phone = Contact.Phone,
                Company = Contact.Company,
                Address = Contact.Address,
                City = Contact.City,
                State = Contact.State,
                ZipCode = Contact.Zip,
                Country = Contact.Country,
                Comments = Contact.Description
            };

            _context.Leads.Add(lead);
            _context.SaveChanges();

            /*var messageToSend = new MimeMessage();

            messageToSend.From.Add(new MailboxAddress("website-lead-no-reply", "akerly.law.lead@gmail.com"));
            //messageToSend.To.Add(new MailboxAddress("leadbox", "info@akerlylaw.com"));
            //messageToSend.To.Add(new MailboxAddress("leadbox", "sakerly@akerlylaw.com"));
            messageToSend.To.Add(new MailboxAddress("leadbox1", "msvajhart@outlook.com"));
            //messageToSend.To.Add(new MailboxAddress("leadbox2", "michael.svajhart@okstate.edu"));
            messageToSend.Subject = "Form Completed By: " + Contact.Name;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Name: {0}", Contact.Name);
            sb.AppendLine();
            if (Contact.Page != null)
            {
                sb.AppendFormat("Page Details: {0}", Contact.Page);
                sb.AppendLine();
            }
            sb.AppendFormat("Email: {0}", Contact.Email);
            sb.AppendLine();
            sb.AppendFormat("Phone: {0}", Contact.Phone);
            sb.AppendLine();
            sb.AppendFormat("Company: {0}", Contact.Company);
            sb.AppendLine();
            sb.AppendFormat("Address: {0} {1} {2} {3} {4}", Contact.Address, Contact.City, Contact.State, Contact.Zip, Contact.Country);
            sb.AppendLine();
            sb.AppendLine("Information:");
            sb.AppendFormat("{0}", Contact.Description);


            messageToSend.Body = new TextPart(TextFormat.Plain) { Text = sb.ToString() };

            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                smtp.Connect("smtp.gmail.com", 587);
                smtp.Authenticate("akerly.law.lead@gmail.com", "Xeruses115");
                try
                {
                    smtp.Send(messageToSend);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                          ex.ToString());
                }
                
                smtp.Disconnect(true);
            }*/
            
        }

    }
}