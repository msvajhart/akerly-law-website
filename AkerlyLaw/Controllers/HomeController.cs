﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AkerlyLaw.Models;
using AkerlyLaw.Helpers;
using AkerlyLaw.DBModels;

namespace AkerlyLaw.Controllers
{
    public class HomeController : Controller
    {
        private readonly CopperheadContext _context;

        public HomeController(CopperheadContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            List<NewsArticleModel> n = NewsHelper.GetTopNews(_context);
            TeamModel t = TeamHelper.GetTeam(_context);
            FirmModel f = LawHelper.GetLaw(_context);
            ATFModel ATF = GetSplash();
            HomePageViewModel model = new HomePageViewModel
            {
                Splash = ATF,
                Team = t,
                Firm = f,
                News = n,
            };

            return View(model);
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ATFModel GetSplash()
        {
            List<SplashModel> Splash = new List<SplashModel>();



            var query = _context.Splash.Where(t => t.WebsiteID == 1);
            foreach (Splash s in query)
            {

                SplashModel splashItem = new SplashModel(s.Title, s.Text);
                Splash.Add(splashItem);
            }

            ATFModel ATF = new ATFModel
            {
                ATF = Splash
            };


            return ATF;
        }


    }
}
