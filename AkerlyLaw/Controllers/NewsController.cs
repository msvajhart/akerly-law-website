﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkerlyLaw.DBModels;
using AkerlyLaw.Helpers;
using AkerlyLaw.Models;
using Microsoft.AspNetCore.Mvc;

namespace AkerlyLaw.Controllers
{
    public class NewsController : Controller
    {
        private readonly CopperheadContext _context;

        public NewsController(CopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            NewsHomeViewModel model = NewsHelper.GetNews(_context);

            return View(model);

        }
        public IActionResult Article(string id)
        {
            NewsArticleModel model = NewsHelper.GetArticle(_context, id);

            if (model == null)
            {
                return RedirectToAction("Index", "News");
            }

            return View(model);
        }
    }
}