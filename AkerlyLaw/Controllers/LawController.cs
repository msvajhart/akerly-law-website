﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AkerlyLaw.Helpers;
using AkerlyLaw.DBModels;
using AkerlyLaw.Models;

namespace AkerlyLaw.Controllers
{
    public class LawController : Controller
    {

        private readonly CopperheadContext _context;

        public LawController(CopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            FirmModel model = LawHelper.GetLaw(_context);

            return View(model);
        }

        public IActionResult Details(string id)
        {
            LawModel Law = LawHelper.GetLawDetails(_context, id);
            LawChildren Children = LawHelper.GetLawChildren(_context, id);
            LawModel before = LawHelper.GetLawDetailsBefore(_context, id); ;
            LawModel after = LawHelper.GetLawDetailsAfter(_context, id); ;
            LawDetailsViewModel model = new LawDetailsViewModel(Law, Children, before, after);

            if (model == null)
            {
                RedirectToAction("Index");
            }

            return View(model);
        }
    }
}