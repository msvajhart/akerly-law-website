﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class LawChildModel
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public LawChildModel(string title, string description)
        {
            Title = title;
            Description = description;
        }
    }
}
