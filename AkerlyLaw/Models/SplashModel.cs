﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class SplashModel
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public SplashModel(string title, string text)
        {
            Title = title;
            Text = text;
        }
    }
}
