﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class TeamCourtsModel
    {
        public string Data { get; set; }

        public TeamCourtsModel(string data)
        {
            Data = data;
        }
    }
}
