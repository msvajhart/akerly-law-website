﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class NewsArticleModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Slug { get; set; }
        public string Author { get; set; }
        public string PreviewText { get; set; }
        public DateTime Date { get; set; }
        public string TypeTag { get; set; }
        public string TypeTitle { get; set; }

        public NewsArticleModel(string title, string content, string preview, string slug, string author, DateTime date, string typetag, string typetitle)
        {
            Title = title;
            Content = content;
            Slug = slug;
            PreviewText = preview;
            Author = author;
            Date = date;
            TypeTag = typetag;
            TypeTitle = typetitle;
        }
    }
}
