﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class HomePageViewModel
    {
        public ATFModel Splash { get; set; }
        public FirmModel Firm { get; set; }
        public TeamModel Team { get; set; }
        public List<NewsArticleModel> News { get; set; }
    }
}
