﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class MemberModel
    {
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Slug { get; set; }
        public string Role { get; set; }
        public string Image { get; set; }
        public string Bio { get; set; }
        public string FocuspointData { get; set; }



        public MemberModel(string name, string nickname, string slug, string role, string image, string focuspointdata, string bio)
        {
            Name = name;
            Nickname = nickname;
            Slug = slug;
            Role = role;
            Image = image;
            FocuspointData = focuspointdata;
            Bio = bio;
        }
    }
}
