﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class TeamMemberViewModel
    {
        public MemberModel Member { get; set; }
        public List<TeamEducationModel> Education { get; set; }
        public List<TeamActivitiesModel> Activities { get; set; }
        public List<TeamCourtsModel> Courts { get; set; }
    }
}
