﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class ContactFormModel
    {
        [Required]
        public string Name { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Company Name")]
        [Required]
        public string Company { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }

        [Display(Name = "Zip Code")]
        [Required]
        public string Zip { get; set; }

        [Required]
        public string Country { get; set; }

        [Display(Name = "How can we help?")]
        [Required]
        public string Description { get; set; }

        public string Page { get; set; }
    }
}
