﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class LawDetailsViewModel
    {
        public LawModel Law { get; set; }
        public LawChildren List { get; set; }

        public LawModel BeforeSlug { get; set; }

        public LawModel AfterSlug { get; set; }

        public LawDetailsViewModel(LawModel law, LawChildren list, LawModel before, LawModel after )
        {
            Law = law;
            List = list;
            BeforeSlug = before;
            AfterSlug = after;
        }
    }
}
