﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class LawModel
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public string ShortDesc { get; set; }
        public string Desc { get; set; }
        public string Slug { get; set; }

        public LawModel(string title, string icon, string shortdesc, string desc, string slug)
        {
            Title = title;
            Icon = icon;
            ShortDesc = shortdesc;
            Desc = desc;
            Slug = slug;
        }
    }
}
