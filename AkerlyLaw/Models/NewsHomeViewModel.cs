﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class NewsHomeViewModel
    {
        public List<NewsArticleModel> News { get; set; }
    }
}
