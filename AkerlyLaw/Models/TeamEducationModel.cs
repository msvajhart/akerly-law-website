﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class TeamEducationModel
    {
        public string University { get; set; }
        public string Extra { get; set; }

        public TeamEducationModel(string university, string extra)
        {
            University = university;
            Extra = extra;
        }
    }
}
