﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.Models
{
    public class TeamActivitiesModel
    {
        public string Data { get; set; }

        public TeamActivitiesModel(string data)
        {
            Data = data;
        }
    }
}

