﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class TeamExtras
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public int TeamID { get; set; }
        public int Category { get; set; }
        public string Data { get; set; }
    }
}
