﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class WebsiteEducation
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string University { get; set; }
        public string Extra { get; set; }
    }
}
