﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class TeamRoles
    {
        public int ID { get; set; }
        public int TeamID { get; set; }
        public int WebsiteRoleID { get; set; }
    }
}
