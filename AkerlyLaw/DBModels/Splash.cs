﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class Splash
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
