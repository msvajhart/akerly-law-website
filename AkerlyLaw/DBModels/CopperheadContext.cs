﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class CopperheadContext : DbContext
    {
        private IConfiguration configuration;

        public CopperheadContext(IConfiguration iConfig)
        {
            configuration = iConfig;
        }

        public DbSet<Team> Team { get; set; }
        public DbSet<TeamEducation> TeamEducation { get; set; }
        public DbSet<TeamRoles> TeamRoles { get; set; }
        public DbSet<WebsiteEducation> WebsiteEducation { get; set; }
        public DbSet<WebsiteRoles> WebsiteRoles { get; set; }
        public DbSet<Law> Law { get; set; }
        public DbSet<LawChildrenDB> LawChildrenDB { get; set; }
        public DbSet<Splash> Splash { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<NewsType> NewsType { get; set; }
        public DbSet<Leads> Leads { get; set; }
        public DbSet<TeamExtras> TeamExtras { get; set; }




        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(configuration.GetSection("ConnectionString").GetSection("DefaultConnection").Value);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Name);
                entity.Property(e => e.Nickname);
                entity.Property(e => e.Slug);
                entity.Property(e => e.ImageName);
                entity.Property(e => e.FocuspointData);
                entity.Property(e => e.Bio);
                entity.Property(e => e.Role);
            });

            modelBuilder.Entity<TeamEducation>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.TeamID);
                entity.Property(e => e.WebsiteEducationID);
            });
            modelBuilder.Entity<TeamRoles>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.TeamID);
                entity.Property(e => e.WebsiteRoleID);
            });

            modelBuilder.Entity<TeamExtras>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.TeamID);
                entity.Property(e => e.Category);
                entity.Property(e => e.Data);
            });

            modelBuilder.Entity<WebsiteEducation>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.University);
                entity.Property(e => e.Extra);

            });
            modelBuilder.Entity<WebsiteRoles>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Name);
            });

            modelBuilder.Entity<Law>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Slug);
                entity.Property(e => e.Title);
                entity.Property(e => e.Icon);
                entity.Property(e => e.ShortDesc);
                entity.Property(e => e.Description);
            });

            modelBuilder.Entity<LawChildrenDB>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.LawID);
                entity.Property(e => e.Title);
                entity.Property(e => e.Description);
            });

            modelBuilder.Entity<Splash>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Title);
                entity.Property(e => e.Text);
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Slug);
                entity.Property(e => e.Published);
                entity.Property(e => e.Title);
                entity.Property(e => e.Content);
                entity.Property(e => e.PreviewText);
                entity.Property(e => e.Author);
                entity.Property(e => e.Date);
                entity.Property(e => e.TypeID);
            });

            modelBuilder.Entity<NewsType>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.TypeTag);
                entity.Property(e => e.TypeTitle);
            });

            modelBuilder.Entity<Leads>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.WebsiteID);
                entity.Property(e => e.Name);
                entity.Property(e => e.Email);
                entity.Property(e => e.Phone);
                entity.Property(e => e.Company);
                entity.Property(e => e.Address);
                entity.Property(e => e.City);
                entity.Property(e => e.State);
                entity.Property(e => e.ZipCode);
                entity.Property(e => e.Country);
                entity.Property(e => e.Company);
                entity.Property(e => e.Comments);
            });

        }
    }
}
