﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class Law
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string ShortDesc { get; set; }
        public string Description { get; set; }
        
    }
}
