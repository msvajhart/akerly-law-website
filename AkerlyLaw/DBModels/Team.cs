﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class Team
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Slug { get; set; }
        public string ImageName { get; set; }
        public string FocuspointData { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }

    }
}
