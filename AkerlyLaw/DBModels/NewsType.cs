﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class NewsType
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string TypeTag { get; set; }
        public string TypeTitle { get; set; }
    }
}
