﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkerlyLaw.DBModels
{
    public class News
    {
        public int ID { get; set; }
        public int WebsiteID { get; set; }
        public string Slug { get; set; }
        public bool Published { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public string PreviewText { get; set; }
        public DateTime Date { get; set; }
        public int TypeID { get; set; }
    }
}
